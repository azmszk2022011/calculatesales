package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店・商品定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	private static final String FILE_NAME_COMMODITY_LIST = "commodity.lst";

	// 支店別・商品別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_CONTINUOUS = "売上ファイル名が連番になっていません";
	private static final String FILE_NOT_ILLEGAL = "の支店コードが不正です";
	private static final String FORMAT_NOT_ILLEGAL = "のフォーマットが不正です";
	private static final String AMOUNT_OVER_ERROR = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if (!readCommodityFile(args[0],FILE_NAME_COMMODITY_LIST, commodityNames)) {
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdfiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();

			if (files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdfiles.add(files[i]);

			}
		}

		Collections.sort(rcdfiles);

		for (int i = 0; i < rcdfiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdfiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdfiles.get(i + 1).getName().substring(0, 8));

			if (latter - former != 1) {
				System.out.println(FILE_NOT_CONTINUOUS);
				return;
			}
		}

		for (int i = 0; i < rcdfiles.size(); i++) {
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdfiles.get(i));
				br = new BufferedReader(fr);
				List<String> rcdSales = new ArrayList<>();

				String line;

				// 一行ずつ読み込む
				while ((line = br.readLine()) != null) {

					rcdSales.add(line);

				}
				if (rcdSales.size() != 3) {
					System.out.println(rcdfiles.get(i).getName() + FORMAT_NOT_ILLEGAL);
					return;

				}
				if (!branchSales.containsKey(rcdSales.get(0))) {
					System.out.println(rcdfiles.get(i).getName() + FILE_NOT_ILLEGAL);
					return;

				}
				if (!rcdSales.get(1).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(rcdSales.get(1));

				Long saleAmount = branchSales.get(rcdSales.get(0)) + fileSale;

				if (saleAmount >= 10000000000L) {
					System.out.println(AMOUNT_OVER_ERROR);
					return;
				}
				branchSales.put(rcdSales.get(0), saleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();

					} catch (IOException e) {

						System.out.println(UNKNOWN_ERROR);
						return;

					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if (!file.exists()) {
				System.out.println(FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				String[] items = line.split(",");

				if ((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println(FILE_INVALID_FORMAT);

					return false;
				}
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 商品コードと商品名を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readCommodityFile(String path, String fileName, Map<String, String> commodityNames) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if (!file.exists()) {
				System.out.println("商品定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				String[] items = line.split(",");

				if ((items.length != 2)|| (!items[0].matches(""))) {
					System.out.println(FILE_INVALID_FORMAT);

					return false;
				}
				commodityNames.put(items[0], items[1]);

			}
			System.out.println(commodityNames);

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//書き込み
			for (String key : branchNames.keySet()) {
				String branchLine = key + "," + branchNames.get(key) + "," + branchSales.get(key);
				bw.write(branchLine);
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
